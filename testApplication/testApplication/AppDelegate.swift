//
//  AppDelegate.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 21.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps

/// Key for Google API
let googleAPIKey = "AIzaSyCv4L8vGWSs3b3Au0cMc5NxMAwSZ8CjipQ"
/// ID of Google search engine
let googleSearchEngineKey = "002785653291323278304:hpbtwfb6xis"


@UIApplicationMain
/**
 Basic AppDelegate class.
*/
class AppDelegate: UIResponder, UIApplicationDelegate {

    /// application's main window
    var window: UIWindow?
    /// application's weather manager
    var weatherManager: WeatherManager!
    /// application's place manager
    var placeManager: PlaceManager!

    /**
     After launch application checks it's database & if it's empty - inserts default data to database.
    */
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        GMSServices.provideAPIKey(googleAPIKey)
        
        let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext
        
        let fetchRequest: NSFetchRequest<Place> = Place.fetchRequest()

        var numberOfPlaces = 0

        do {
            numberOfPlaces = try managedObjectContext.count(for: fetchRequest)
        } catch {
            fatalError("Number of places in database could not be checked.")
        }

        
        if numberOfPlaces == 0 {
            let placesDescriptions: [(name: String, googleID: String, latitude: Double, longitude: Double, imageURL: String)] = [
                (name: "London",   googleID: "ChIJdd4hrwug2EcRmSrV3Vo6llI", latitude: 51.50853,  longitude: -0.12574, imageURL: "http://assets.fodors.com/destinations/2869/tower-bridge-london-england_main.jpg"),
                (name: "Tokyo",    googleID: "ChIJXSModoWLGGARILWiCfeu2M0", latitude: 35.689499, longitude: 139.691711, imageURL: "http://dvphoenix.ru.images.1c-bitrix-cdn.ru/upload/iblock/b1d/b1d2bbcc113d0a602ff8fada5bb934d8.jpg?1419987104560662"),
                (name: "New York", googleID: "ChIJOwg_06VPwokRYv534QaPC8g", latitude: 43.000351, longitude: -75.499901, imageURL: "https://media-cdn.tripadvisor.com/media/photo-s/03/9b/2d/f2/new-york-city.jpg")
            ]
            
            for (index, placeDescription) in placesDescriptions.enumerated() {
                let newPlace = NSEntityDescription.insertNewObject(forEntityName: Place.entityName, into: managedObjectContext) as! Place
                newPlace.googleID = placeDescription.googleID
                newPlace.name    = placeDescription.name
                newPlace.latitude = NSNumber(value: placeDescription.latitude)
                newPlace.longitude = NSNumber(value: placeDescription.longitude)
                newPlace.imageGlobalURL = placeDescription.imageURL
                newPlace.interfaceIndex = index as NSNumber?
                newPlace.isCurrentPlace = false
            }
            
            managedObjectContext.safeSave(reset: true)
        }
        
        GoogleSearchClient.sharedClient = GoogleSearchClient(APIKey: googleAPIKey, engineID: googleSearchEngineKey)
        
        placeManager = PlaceManager(managedObjectContext: managedObjectContext)
        PlaceManager.sharedManager = placeManager
        placeManager.startPlaceObserving()
        
        weatherManager = WeatherManager(placeManager: placeManager)
        placeManager.addObserver(weatherManager)
        weatherManager.startWeatherObserving()
        weatherManager.fireObserveOperation()
        
        return true
    }

}

