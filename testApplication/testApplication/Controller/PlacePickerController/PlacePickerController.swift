//
//  PlacePickerController.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 24.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps

/**
 PlacePickerController presents some special interface. User could search & pick a new place to his list via this interface..
*/
class PlacePickerController: UIViewController {
    /// Places search bar.
    @IBOutlet var searchBar: UISearchBar!
    /// Table view with place suggestions.
    @IBOutlet var tableView: UITableView!
    
    /// Current available suggestions.
    var currentSuggestions = [(suggestion: String, googleID: String)]()
    /// Google client which is used to search places.
    var placesClient = GMSPlacesClient()
    /// Managed object to put new place in.
    var managedObjectContext: NSManagedObjectContext!
    
    /// Some preparations after view is loaded.
    override func viewDidLoad() {
        super.viewDidLoad()
        for subView in searchBar.subviews {
            for secondLevelSubview in subView.subviews {
                if secondLevelSubview is UITextField {
                    (secondLevelSubview as! UITextField).textColor = UIColor.white
                }
            }
        }
        
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    /// On **viewWillAppear** keyboard automatically appears too.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchBar.becomeFirstResponder()
    }
}
