//
//  PlacePickerController+SearchBar.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 28.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps

extension PlacePickerController: UISearchBarDelegate {
    // MARK: - UISearchBarDelegate
    /// Sent when user taps "cancel" on search bar.
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        performSegue(withIdentifier: "backToPlaceList", sender: self)
    }
    
    /// Sent when user changes current search string.
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        
        guard let searchText = searchBar.text, searchText.characters.count > 0 else {
            currentSuggestions.removeAll()
            tableView.reloadData()
            return
        }
        
        // first - check cache
        let fetchRequest: NSFetchRequest<PlaceSearch> = PlaceSearch.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "searchString like[cd] %@", searchText)
        
        do {
            let searchResults = try managedObjectContext.fetch(fetchRequest)
            if searchResults.count > 0 {
                currentSuggestions = searchResults.map{ return (suggestion: $0.resultString, googleID: $0.googleID) }
                tableView.reloadData()
                return
            }
        } catch {
            debugLog("Error during fecthing search results: \(error)")
            return
        }
        
        let filter = GMSAutocompleteFilter()
        filter.type = GMSPlacesAutocompleteTypeFilter.city
        placesClient.autocompleteQuery(searchText, bounds: nil, filter: filter, callback: placeSearchDidFinish)
    }
    
    /// Called by Google service when autocomplete predications are ready.
    private func placeSearchDidFinish(_ autocompletePredictions: [GMSAutocompletePrediction]?, error: Error?){
        guard error == nil else {
            debugLog("error is not nil: \(error)")
            return
        }
        let autocompletes = autocompletePredictions?.filter({$0.placeID != nil}) ?? [GMSAutocompletePrediction]()
        if autocompletes.count > 0 {
            managedObjectContext.perfomBlockOnPrivateContextAndWait{ managedObjectContext in
                for autocomplete in autocompletes {// Save results to cache
                    let newSearch = NSEntityDescription.insertNewObject(forEntityName: PlaceSearch.entityName, into: managedObjectContext) as! PlaceSearch
                    newSearch.searchString = self.searchBar.text ?? "error"
                    newSearch.resultString = autocomplete.attributedFullText.string
                    newSearch.googleID     = autocomplete.placeID!
                }
                
                //clean cache from old results
                let fetchRequest: NSFetchRequest<PlaceSearch> = PlaceSearch.fetchRequest()
                fetchRequest.predicate = NSPredicate(format: "createdOnClient < %@", Date().addingTimeInterval(-(24 * 60 * 60)) as CVarArg)
                
                do {
                    let cacheResults = try managedObjectContext.fetch(fetchRequest)
                    if cacheResults.count > 0 {
                        cacheResults.forEach{
                            managedObjectContext.delete($0)
                        }
                    }
                } catch {
                    debugLog("Error during fecthing cache results: \(error)")
                    return
                }
            }
        }
        currentSuggestions = autocompletes.map{ return (suggestion: $0.attributedFullText.string, googleID: $0.placeID!) }
        tableView.reloadData()
    }
}
