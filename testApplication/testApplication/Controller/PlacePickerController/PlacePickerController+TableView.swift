//
//  PlacePickerController+TableView.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 28.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps

extension PlacePickerController: UITableViewDataSource, UITableViewDelegate {
    //MARK: - UITableViewDataSource, UITableViewDelegate
    /// Returns number of rows in table's section.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentSuggestions.count
    }
    
    /// Returns cell for some index path.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestionPlaceCell") as! PlaceSuggestionCell
        
        let suggestion = currentSuggestions[indexPath.row]
        
        let placeString = NSMutableAttributedString(string: suggestion.suggestion)
        
        placeString.setAttributes([NSForegroundColorAttributeName : UIColor.lightText,
            NSFontAttributeName : UIFont.systemFont(ofSize: 17.0) ], range: NSMakeRange(0,placeString.length) )
        
        if let searchText = searchBar.text, searchText.characters.count > 0 {
            let searchRange = (placeString.string as NSString).range(of: searchText)
            placeString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 17.0), NSForegroundColorAttributeName : UIColor.white], range: searchRange)
        }
        
        
        cell.textView.attributedText = placeString
        
        return cell
    }
    
    /// Called when user select some cell in table.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let placeGoogleID = currentSuggestions[indexPath.row].googleID
        
        if PlaceManager.sharedManager.placeByGoogleID(placeGoogleID) != nil {
            performSegue(withIdentifier: "backToPlaceList", sender: self)
        } else {
            placesClient.lookUpPlaceID(placeGoogleID, callback: placeCoordinatesSearchFinished)
        }
    }
    
    /// Called by Google service when it has finished search for some place.
    private func placeCoordinatesSearchFinished(_ place: GMSPlace?, error: Error?){
        guard error == nil else {
            debugLog("error is not nil: \(error!)")
            return
        }
        
        guard let place = place else {
            debugLog("place is nil")
            return
        }
        
        PlaceManager.sharedManager.insertNewPlace(place)
        
        performSegue(withIdentifier: "backToPlaceList", sender: self)
    }
}
