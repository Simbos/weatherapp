//
//  PlaceListController+TableView.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 28.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

extension PlaceListController: UITableViewDataSource, UITableViewDelegate {
    // MARK: - UITableViewDataSource, UITableViewDelegate
    /**
     Used to configurate cell for some index path.
     - parameter cell: cell to be configurated.
     - parameter indexPath: index path where cell should be presented.
    */
    func configureCell(_ cell: UITableViewCell, atIndexPath indexPath: IndexPath){
        let cell = cell as! PlaceWeatherCell
        let place: Place?
        if indexPath.row == 0 {
            place = placeManager.currentPlace
        } else {
            place = placeManager.observablePlaces[indexPath.row - 1]
        }
        
        cell.placeNameLabel.text = place?.name ?? ""
        cell.placeImageView.image = place?.image
        
        var currentWeather: Weather?
        
        if let setOfWeathers = place?.forecast as? Set<Weather> {
            
            let filterDate = Date(timeInterval: -(3 * 60 * 60), since: Date())
            let display: [(temperature: Double, datetime: Date)] = setOfWeathers.filter({ ($0.forecastDate.timeIntervalSince(filterDate) > 0.0) && ($0.forecastDate.timeIntervalSince(filterDate) < (18 * 60 * 60)) }).map{ return (temperature: $0.temperatureIn(.celsius), datetime: $0.forecastDate) }
            
            cell.weatherChart.redrawChart(display)
        } else {
            cell.weatherChart.freeChart()
        }
        
        _ = (place?.forecast as? Set<Weather>).map{
            currentWeather = $0.min(by: { (element1, element2) -> Bool in
                return element1.forecastDate.compare(element2.forecastDate as Date) != ComparisonResult.orderedDescending
            })
        }
        if currentWeather != nil {
            let temperature = currentWeather!.temperatureIn(.celsius)
            var temperatureString = String(format:"%.1f%@",temperature, TemperatureUnit.celsius.unitString)
            if temperature > 0 {
                temperatureString = "+" + temperatureString
            }
            cell.currentTemperatureLabel.text = temperatureString
        } else {
            cell.currentTemperatureLabel.text = ""
        }
    }
    
    /// Returns number of rows for table section.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeManager.observablePlaces.count + 1
    }
    
    /// Returns cell wich should be presented at some index path.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceWeatherCell") as! PlaceWeatherCell
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    /// Returns footer view for table.
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = WeatherFooter.loadFromNib()
        footer.addButton.addTarget(self, action: #selector(PlaceListController.pickNewPlace), for: .touchUpInside)
        footer.editButton.addTarget(self, action: #selector(PlaceListController.switchMoveMode), for: .touchUpInside)
        return footer
    }
    
    // editing
    /// Returns false - table doesn't insert indent view during editing.
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    /// Returns .None on editing style, otherwise returns .Delete.
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if tableView.isEditing {
            return .none
        } else {
            return . delete
        }
    }
    
    /// Returns **true** for all cells except first, cause current place cell should not be editable.
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return (indexPath.row != 0)
    }
    
    /// Removes place when user taps "delete"
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            placeManager.removePlaceFromPosition(indexPath.row - 1)
        default:
            ()
        }
    }
    // moving
    /// Returns **true** for all cells except first, cause current place cell should not be movable.
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return (indexPath.row != 0)
    }
    
    /// Called shen user has moved cell to new destination.
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        placeManager.changePlaceInterfacePosition(sourceIndexPath.row - 1, toPosition: destinationIndexPath.row - 1)
    }
    
    /// Returns **proposedDestinationIndexPath** for all moves except this destination is current place cell.
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        if proposedDestinationIndexPath.row == 0 {
            return sourceIndexPath
        } else {
            return proposedDestinationIndexPath
        }
    }
}
