//
//  PlaceListController.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 21.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

/**
 ## PlaceListController
 PlaceListController ouputs current user's location weather data & all weather data for all places wich are stored on device.
*/
class PlaceListController: UIViewController {
    /// Keeps background image.
    @IBOutlet var backgroundImageView: UIImageView!
    /// Table view with cells, each cell presents weather data for some place.
    @IBOutlet var tableView: UITableView!
    /// Observable managed object context.
    var managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext
    /// Observable place manager
    var placeManager: PlaceManager!

    /// Some preparations after view is loaded.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        placeManager = PlaceManager.sharedManager
        placeManager.addObserver(self)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        backgroundImageView.image = Gradient.gradientImageWithSize(view.frame.size, colors: [view.backgroundColor!,UIColor(red: 0.0, green: 0.0, blue: 119.0, alpha: 1.0),view.backgroundColor!])
    }
    
    /// Some preparations after view appeared.
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        CurrentLocationManager.sharedManager.checkLocationManager()
    }
    
    /// Preparations before some segue transitions
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let segueID = segue.identifier else {
            return
        }
        
        switch segueID {
        case "PickNewPlace":
            if let destinationViewController = segue.destination as? PlacePickerController {
                destinationViewController.managedObjectContext = self.managedObjectContext
            }
        default:
            ()
        }
    }
    
    /// Called by unwind segues.
    @IBAction func unwindToPlaceList(_ segue: UIStoryboardSegue){
        
    }
    
    /// Switches "move" mode of table's cells.
    func switchMoveMode(){
        let moveModeEnabled = tableView.isEditing
        tableView.setEditing(!moveModeEnabled, animated: true)
    }
    
    /// Called to pick a new place
    func pickNewPlace(){
        self.performSegue(withIdentifier: "PickNewPlace", sender: self)
    }
}






