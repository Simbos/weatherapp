//
//  PlaceListController+PlaceManagerObserver.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 28.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import Foundation

extension PlaceListController: PlaceManagerObserver {
    // MARK: - PlaceManagerObserver
    /// sent every time when place manager wants to modify it's context.
    func placeManagerWillChangeContent(_ placeManager: PlaceManager) {
        tableView.beginUpdates()
    }
    
    /// sent every time when place manager has just modified it's context.
    func placeManagerDidChangeContent(_ placeManager: PlaceManager) {
        tableView.endUpdates()
    }
    
    /// sent every time when place manager has change current place.
    func placeManager(_ placeManager: PlaceManager, currentPlaceWasChanged currentPlace: Place?) {
        let tableIndexPath = IndexPath(row: 0, section: 0)
        let cell = tableView.cellForRow(at: tableIndexPath)!
        configureCell(cell, atIndexPath: tableIndexPath)
    }
    
    /// sent every time when place manager updates data of current place.
    func placeManager(_ placeManager: PlaceManager, currentPlaceWasUpdated currentPlace: Place?) {
        let tableIndexPath = IndexPath(row: 0, section: 0)
        if let cell = tableView.cellForRow(at: tableIndexPath){
            configureCell(cell, atIndexPath: tableIndexPath)
        }
    }
    
    /// sent every time when place manager inserts new place to it's context.
    func placeManager(_ placeManager: PlaceManager, newPlaceWasInserted newPlace: Place, atInterfaceIndex interfaceIndex: Int){
        let tableNewIndexPath = IndexPath(row: interfaceIndex + 1, section: 0)
        tableView.insertRows(at: [tableNewIndexPath], with: .fade)
    }
    
    /// sent every time when place manager removes place from it's context.
    func placeManager(_ placeManager: PlaceManager, placeWasRemoved place: Place, atInterfaceIndex interfaceIndex: Int){
        let tableIndexPath = IndexPath(row: interfaceIndex + 1, section: 0)
        tableView.deleteRows(at: [tableIndexPath], with: .fade)
    }
    
    /// sent every time when place manager changes interfave index of place.
    func placeManager(_ placeManager: PlaceManager, placeWasMoved place: Place, fromInterfaceIndex: Int, toInterfaceIndex: Int){
        let tableIndexPath = IndexPath(row: fromInterfaceIndex + 1, section: 0)
        let tableNewIndexPath = IndexPath(row: toInterfaceIndex + 1, section: 0)
        
        tableView.deleteRows(at: [tableIndexPath], with: .fade)
        tableView.insertRows(at: [tableNewIndexPath], with: .fade)
    }
    
    /// sent every time when place manager updates data of some place.
    func placeManager(_ placeManager: PlaceManager, placeWasUpdated place: Place, atInterfaceIndex: Int){
        let tableIndexPath = IndexPath(row: atInterfaceIndex + 1, section: 0)
        if let cell = tableView.cellForRow(at: tableIndexPath){
            configureCell(cell, atIndexPath: tableIndexPath)
        }
    }
}
