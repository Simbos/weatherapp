//
//  Gradient.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 29.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import Foundation

/**
 ## Gradient
 Gradient is used to produce gradient images.
*/
class Gradient {
    /**
     Creates a simple vertical gradient.
     - parameter imageSize: prefer size of image.
     - parameter colors: array of color from up to down to use
     - returns: specified gradient image
    */
    class func gradientImageWithSize(_ imageSize: CGSize, colors: [UIColor]) -> UIImage {
        
        let rawArrayColors:[[CGFloat]] = colors.map{ color in
            var sourceRed = CGFloat(0)
            var sourceGreen = CGFloat(0)
            var sourceBlue = CGFloat(0)
            var sourceAlpha = CGFloat(0)
            color.getRed(&sourceRed, green: &sourceGreen, blue: &sourceBlue, alpha: &sourceAlpha)
            return [sourceRed, sourceGreen, sourceBlue, sourceAlpha]
        }
        
        let rawColors: [CGFloat] = rawArrayColors.flatMap{ $0 }
        
        let baseSpace = CGColorSpaceCreateDeviceRGB()
        let gradient = CGGradient(colorSpace: baseSpace, colorComponents: rawColors, locations: nil, count: colors.count);
        
        UIGraphicsBeginImageContextWithOptions(imageSize, false, UIScreen.main.nativeScale)
        let context = UIGraphicsGetCurrentContext()!
        
        let rect = CGRect(origin: CGPoint.zero, size: imageSize)
        
        let startPoint = CGPoint(x: rect.midX, y: rect.minY)
        let endPoint = CGPoint(x: rect.midX, y: rect.maxY)
        
        context.drawLinearGradient(gradient!, start: startPoint, end: endPoint, options: [])

        context.strokePath()
        
        let gradientImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return gradientImage!
    }
}
