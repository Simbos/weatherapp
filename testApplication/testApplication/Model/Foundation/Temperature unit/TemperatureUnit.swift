//
//  TemperatureUnit.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 29.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import Foundation

/**
 Available temperature's units.
 * celsius
 * kelvin
 * fahrenheit
 */
enum TemperatureUnit {
    /// Celsius are used in Europe.
    case celsius
    /// Kelvins are used in science.
    case kelvin
    /// Fahrenheits are used in USA.
    case fahrenheit
    
    /// string which is usually typed after value.
    var unitString: String {
        switch self {
        case .celsius:
            return "°C"
        case .kelvin:
            return "°K"
        case .fahrenheit:
            return "°F"
        }
    }
}
