//
//  Debug.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 11.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import Foundation

/**
 This function is used for all debug ouputs. It outputs message, filename, function name and line where it was called.
 */
func debugLog(_ message: String,
              file: StaticString = #file,
              function: StaticString = #function,
              line: Int = #line){
    
    let output: String
    if let filename = URL(string:String(describing: file))?.lastPathComponent {
        output = "\(filename) \(function) line \(line) : \(message)"
    } else {
        output = "\(file).\(function) line \(line) : \(message)"
    }
    NSLog("%@", output)
}
