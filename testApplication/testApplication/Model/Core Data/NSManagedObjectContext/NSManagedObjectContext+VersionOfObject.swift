//
//  NSManagedObjectContext+VersionOfObject.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 28.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import CoreData

extension NSManagedObjectContext{
    /**
     Returns version of managed object from other context.
     - parameter managedObject: managed object to search for.
     - returns: found object or **nil**
    */
    func versionOfObject<T: NSManagedObject>(_ managedObject: T) -> T? {
        return object(with: managedObject.objectID) as? T
    }
}

extension NSManagedObject {
    /**
     Returns version of this object from other context.
     - parameter managedObjectContext: managed object context to search in.
     - returns: found object or **nil**
    */
    func versionFor(_ managedObjectContext: NSManagedObjectContext) -> Self? {
        return managedObjectContext.versionOfObject(self)
    }
}
