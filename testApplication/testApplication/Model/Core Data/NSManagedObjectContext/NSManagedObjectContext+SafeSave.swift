//
//  NSManagedObjectContext+SafeSave.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 23.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import CoreData

extension NSManagedObjectContext {
    /**
     Tries to save changes in context if it needed. Also it could reset context after procedure.
     - parameter reset: set **true** if you want to reset context after saving.
    */
    func safeSave(reset: Bool = false){
        if hasChanges {
            do {
                try save()
                if reset {
                    self.reset()
                }
            } catch {
                fatalError("managedObjectContext can't save context:\(error)")
            }
        }
    }
}
