//
//  NSManagedObjectContext+PrivateContext.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 28.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import CoreData

extension NSManagedObjectContext{
    /**
     Performs some changes on context via private subcontext. After that it saves & resets changes in managed object context.
     - parameter block: block of changes.
    */
    func perfomBlockOnPrivateContext(_ block:@escaping (_ managedObjectContext: NSManagedObjectContext) -> Void){
        let operationContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        operationContext.parent = self
        operationContext.perform {
            block(operationContext)
            operationContext.safeSave()
            self.perform{
                self.safeSave(reset: true)
            }
        }
    }
    
    /**
     Performs some changes on context via private subcontext. After that it saves & resets changes in managed object context. Synchronous method.
     - parameter block: block of changes.
    */
    func perfomBlockOnPrivateContextAndWait(_ block:@escaping (_ managedObjectContext: NSManagedObjectContext) -> Void){
        let operationContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        operationContext.parent = self
        operationContext.performAndWait {
            block(operationContext)
            operationContext.safeSave()
            self.perform{
                self.safeSave(reset: true)
            }
        }
    }
}
