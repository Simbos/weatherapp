//
//  Place.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 23.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreData

/**
 Place represent some city or othe administrative unit in database.
*/
class Place: NSManagedObject {
    /// Name of entity for this class in all managed object contexts.
    static let entityName = "Place"
    
    /**
     **image** is a countable property. In fact **Place**-object keeps only filename of local stored image. But it could be easily get by filename. On write operation, object stores file in local directory and generate it's filename.
    */
    var image: UIImage? {
        get {
            guard imageFileName != nil else {
                debugLog("'Place.image' is nil")
                return nil
            }
            
            let urlToRead = type(of: self).imagesDirectory.appendingPathComponent(imageFileName!)

            let filePath = urlToRead.path
            
            guard !filePath.isEmpty else {
                debugLog("can't get filepath from fileURL '\(urlToRead)'")
                return nil
            }
            
            if let image = UIImage(contentsOfFile: filePath) {
                return image
            } else {
                debugLog("can't get image from filePath '\(filePath)'")
                return nil
            }
        }
        
        set {
            guard newValue != nil else {
                debugLog("new value is nil")
                return
            }
            
            guard let fileData = UIImageJPEGRepresentation(newValue!, 1.0) else {
                debugLog("Can't get image data")
                return
            }
            
            let filename = NSUUID().uuidString.lowercased() + ".jpg"
            let urlToWrite = type(of: self).imagesDirectory.appendingPathComponent(filename)
            
            if !FileManager.default.fileExists(atPath: type(of: self).imagesDirectory.path){
                do {
                    try FileManager.default.createDirectory(at: type(of: self).imagesDirectory, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    debugLog("\(error)")
                }
            }
            
            do {
                try fileData.write(to: urlToWrite, options: NSData.WritingOptions.noFileProtection)
                imageFileName = filename
            } catch {
                debugLog("\(error)")
            }
        }
    }
    
    /**
     This URL is used to store image files on device.
    */
    private static let imagesDirectory: URL = {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let imagesURL = documentsURL.appendingPathComponent("Images")
        return imagesURL
    }()
    
    // MARK: Class functions.
    
    /**
     Returns max available interface index of place in some context.
     - parameter managedObjectContext: context to search in.
     - returns: max available interface index or **nil**(if there are no places with interface index).
    */
    class func maxInterfaceIndexInContext(_ managedObjectContext: NSManagedObjectContext) -> Int? {
        let request = NSFetchRequest<NSFetchRequestResult>()
        request.entity = NSEntityDescription.entity(forEntityName: Place.entityName, in: managedObjectContext)
        request.resultType = .dictionaryResultType

        let keyPathExpression = NSExpression(forKeyPath: "interfaceIndex")
        let maxExpression = NSExpression(forFunction: "max:", arguments: [keyPathExpression])
        
        let expressionDescription = NSExpressionDescription()
        expressionDescription.name = "maxInterfaceIndex"
        expressionDescription.expression = maxExpression
        expressionDescription.expressionResultType = .integer16AttributeType
        
        request.propertiesToFetch = [expressionDescription]
        
        do {
            if let results = try managedObjectContext.fetch(request) as? [[String : AnyObject]] {
                return results.first?["maxInterfaceIndex"] as? Int
            }
        } catch {
            debugLog("Error during fetching max interface index: \(error)")
        }
        return nil
    }
    
    /**
     Returns place which is marked as current in context.
     - parameter managedObjectContext: context to search in.
     - returns: current place or **nil** (if there are no places marked as current).
    */
    class func currentPlaceInContext(_ managedObjectContext: NSManagedObjectContext) -> Place? {
        let request: NSFetchRequest<Place> = Place.fetchRequest()
        request.predicate = NSPredicate(format: "isCurrentPlace == true")
        
        do{
            let places = try managedObjectContext.fetch(request)
            return places.first
        } catch {
            debugLog("Error during fetching current place: \(error)")
        }
        return nil
    }
    
    /**
     Returns place with specified Google identificator from context.
     - parameter managedObjectContext: context to search in.
     - parameter googleID: Google identificator of place.
     - returns: specified place or **nil**(if it wasn't found in context).
    */
    class func placeInContext(_ managedObjectContext: NSManagedObjectContext, withGoogleID googleID: String) -> Place? {
        let fetchRequest: NSFetchRequest<Place> = Place.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "googleID == %@", googleID)
        
        do {
            let places = try managedObjectContext.fetch(fetchRequest)
            return places.first
        } catch {
            debugLog("Context throwed an error during fecthing place with googleID = '\(googleID)', \(error)")
        }
        return nil
    }
    
}
