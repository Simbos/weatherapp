//
//  Place+CoreDataProperties.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 23.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Place {
    @nonobjc class func fetchRequest() -> NSFetchRequest<Place> {
        return NSFetchRequest<Place>(entityName: "Place")
    }

    /// Google places identificator.
    @NSManaged var googleID: String
    /// URL of place's image (if it exists).
    @NSManaged var imageGlobalURL: String?
    /// Filename of image (if it was downloaded).
    @NSManaged var imageFileName: String?
    /// Interface index (if place is presented in interface).
    @NSManaged var interfaceIndex: NSNumber?
    /// **true** if user is in this place a the moment. **false** otherwise.
    @NSManaged var isCurrentPlace: NSNumber
    /// latitide of place.
    @NSManaged var latitude: NSNumber
    /// longitude of place.
    @NSManaged var longitude: NSNumber
    /// name of place.
    @NSManaged var name: String
    /// set of weather objects stored for this place
    @NSManaged var forecast: NSSet?
}
