//
//  Weather.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 23.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreData

/**
 Represents some kind of weather measurements or forecasts.
*/
class Weather: NSManagedObject {
    
    /// Name of entity for this class in all managed object contexts.
    static let entityName = "Weather"

    /// After insert object automatically generate **createdOnClient** value.
    override func awakeFromInsert() {
        super.awakeFromInsert()
        createdOnClient = Date()
    }
    
    /**
     Presents temperature in some units.
     - parameter tempretureUnit: temperature units(Celsius,Kelvin,etc.)
    */
    func temperatureIn(_ tempretureUnit: TemperatureUnit) -> Double {
        switch tempretureUnit {
        case .kelvin:
            return temperature.doubleValue
        case .celsius:
            return temperature.doubleValue - 273.15
        case .fahrenheit:
            return 1.8 * temperatureIn(.celsius) + 32.0
        }
    }

}
