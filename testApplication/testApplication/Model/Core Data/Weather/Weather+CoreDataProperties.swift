//
//  Weather+CoreDataProperties.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 23.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Weather {
    @nonobjc class func fetchRequest() -> NSFetchRequest<Weather> {
        return NSFetchRequest<Weather>(entityName: "Weather")
    }

    /// Date when object was created on device.
    @NSManaged var createdOnClient: Date
    /// Date of forecast
    @NSManaged var forecastDate: Date
    /// temperature, Kelvins
    @NSManaged var temperature: NSNumber
    /// Related place.
    @NSManaged var place: Place
}
