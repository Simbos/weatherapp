//
//  PlaceSearch+CoreDataProperties.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 24.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PlaceSearch {
    @nonobjc class func fetchRequest() -> NSFetchRequest<PlaceSearch> {
        return NSFetchRequest<PlaceSearch>(entityName: "PlaceSearch")
    }

    /// Google identificator of place.
    @NSManaged var googleID: String
    /// date when the object was created on device.
    @NSManaged var createdOnClient: Date
    /// Name of place presented by Google.
    @NSManaged var resultString: String
    /// Original user's query.
    @NSManaged var searchString: String
}
