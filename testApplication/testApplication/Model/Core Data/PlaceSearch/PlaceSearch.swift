//
//  PlaceSearch.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 24.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreData

/**
 We need to store some search results of Google Places API cause Application have some limits of queries.
 **PlaceSearch** stores some result of place searching.
*/
class PlaceSearch: NSManagedObject {
    
    /// Name of entity for this class in all managed object contexts.
    static let entityName = "PlaceSearch"

    /// After insert object automatically generate **createdOnClient** value.
    override func awakeFromInsert() {
        super.awakeFromInsert()
        createdOnClient = Date()
    }

}
