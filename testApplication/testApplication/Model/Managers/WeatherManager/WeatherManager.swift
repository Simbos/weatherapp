//
//  WeatherManager.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 23.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import Foundation
import RestKit

/// main URL of "Open Weather Map" API.
private let apiWeatherURL = "http://api.openweathermap.org/data/2.5"
/// path for current weather operations.
private let weatherPath   = "weather"
/// path for forecasts operations.
private let forecastPath  = "forecast"
/// App's key for "Open Weather Map" API
private let keyAPI        = "11d217aaf9273ccdc893f6b80c059e1c"

/**
 ## Weather manager.
 Weather manager oberves places of come **PlaceManager**, downloads & parse weather information to database.
*/
class WeatherManager: NSObject {
    /// example place manager wich is observable by weather manager.
    private let placeManager: PlaceManager
    /// timer wich fires the weather reloading operation.
    private var observeTimer: Timer?
    
    /// init weather manager with place manager.
    init(placeManager: PlaceManager){
        self.placeManager = placeManager
        
        super.init()
        
        let baseURL = URL(string: apiWeatherURL)
        let client = AFHTTPClient(baseURL: baseURL)
        let objectManager = RKObjectManager(httpClient: client)
        
        let managedObjectStore = RKManagedObjectStore(managedObjectModel: CoreDataManager.sharedInstance.managedObjectModel.mutableCopy() as! NSManagedObjectModel)
        objectManager?.managedObjectStore = managedObjectStore
        
        RKObjectManager.setShared(objectManager)
        
        prepareForecastDescriptor()
        prepareCurrentWeatherDescriptor()
    }
    
    /**
     This private method is called during object initialization.
     It prepeares response descriptor for **forecast** endpoints.
    */
    private func prepareForecastDescriptor(){
        let objectManager = RKObjectManager.shared()
        
        let weatherMapping = RKEntityMapping(forEntityForName: Weather.entityName, in: objectManager?.managedObjectStore)
        weatherMapping?.addAttributeMappings(from: ["dt" : "forecastDate", "main.temp" : "temperature"])
        
        let placeMapping = RKEntityMapping(forEntityForName: Place.entityName, in: objectManager?.managedObjectStore)
        placeMapping?.identificationAttributes = ["googleID"]
        placeMapping?.addAttributeMappings(from: ["googleID" : "googleID"])
        placeMapping?.shouldMapRelationshipsIfObjectIsUnmodified = true
        
        let placeWeatherMapping = RKRelationshipMapping(fromKeyPath: "list", toKeyPath: "forecast", with: weatherMapping)
        placeWeatherMapping?.assignmentPolicy = .assignmentPolicyReplace 
        
        placeMapping?.addPropertyMapping(placeWeatherMapping)
        let successStatusCodes = RKStatusCodeIndexSetForClass(.successful)
        
        let responseDescriptor = RKResponseDescriptor(mapping: placeMapping, method: .GET, pathPattern: forecastPath, keyPath: nil, statusCodes: successStatusCodes)
        
        objectManager?.addResponseDescriptor(responseDescriptor)
    }
    
    /**
     This private method is called during object initialization.
     It prepeares response descriptor for **weather** endpoints.
     */
    private func prepareCurrentWeatherDescriptor(){
        let objectManager = RKObjectManager.shared()
        
        let weatherMapping = RKEntityMapping(forEntityForName: Weather.entityName, in: objectManager?.managedObjectStore)
        weatherMapping?.identificationAttributes = ["forecastDate"]
        weatherMapping?.identificationPredicateBlock = { dictionary, context in            
                return  NSPredicate(format: "place.googleID == %@", dictionary?["googleID"] as! String)
        }
        weatherMapping?.addAttributeMappings(from: ["dt" : "forecastDate", "main.temp" : "temperature"])
        
        let placeMapping = RKEntityMapping(forEntityForName: Place.entityName, in: objectManager?.managedObjectStore)
        placeMapping?.identificationAttributes = ["googleID"]
        placeMapping?.addAttributeMappings(from: ["googleID" : "googleID"])
        
        let weatherPlaceMapping = RKRelationshipMapping(fromKeyPath: nil, toKeyPath: "place", with: placeMapping)
        
        weatherMapping?.addPropertyMapping(weatherPlaceMapping)
        
        let successStatusCodes = RKStatusCodeIndexSetForClass(.successful)
        
        let responseDescriptor = RKResponseDescriptor(mapping: weatherMapping, method: .GET, pathPattern: weatherPath, keyPath: nil, statusCodes: successStatusCodes)
        
        objectManager?.addResponseDescriptor(responseDescriptor)
    }

    private var isWeatherObserving = false
    /**
     This method should be called when manager is ready to observe places.
    */
    func startWeatherObserving(){
        if !isWeatherObserving {
            isWeatherObserving = true
            observeTimer = Timer.scheduledTimer(timeInterval: 600.0, target: self, selector: #selector(WeatherManager.fireObserveOperation), userInfo: nil, repeats: true)
        }
    }
    
    /// temprary buffer of places for observe cycle.
    private var placesToObserve = [Place]()
    /// indicates whenever manager is observing places right at the moment.
    private(set) var isObserving = false
    /// this flag is used to indicate manager that it should start reobserve operation as soon as possible.
    private var needReobserve = false
    
    /**
     This method should be called to start observe operation right now.
    */
    func fireObserveOperation(){
        guard !isObserving else {
            needReobserve = true
            return
        }
        
        isObserving = true
        placesToObserve = placeManager.observablePlaces
        if placeManager.currentPlace != nil {
            placesToObserve.append(placeManager.currentPlace!)
        }

        observePlaces()
    }
    
    /**
     This method is called to prepare observe operation.
    */
    private func observePlaces(){
        guard placesToObserve.count > 0 else {
            isObserving = false
            if needReobserve {
                needReobserve = false
                fireObserveOperation()
            }
            return
        }
        
        let place = placesToObserve.removeFirst().versionFor(placeManager.managedObjectContext)!
        
        updateForecastsForPlace(place)
    }
    
    /**
     This method is called first for each place. It makes a request for **forecasts** API & replace all Weather-Objects of places.
     - parameter place: place wich forecast-set should be updated.
    */
    private func updateForecastsForPlace(_ place: Place){
        
        let placeGoogleID = place.googleID
        
        let objectManager = RKObjectManager.shared()
        
        let urlRequest = objectManager?.request(with: nil, method: .GET, path: forecastPath, parameters: ["lat": place.latitude, "lon": place.longitude, "appid": keyAPI])

        let operation = objectManager?.managedObjectRequestOperation(with: urlRequest as URLRequest!, managedObjectContext: placeManager.managedObjectContext, success: { (objectRequestOperation: RKObjectRequestOperation?, mappingResult: RKMappingResult?) in
            if objectRequestOperation == nil {
                debugLog("objectRequestOperation == nil")
            }
            if mappingResult == nil {
                debugLog("mappingResult == nil")
            }
            self.updateCurrentWeatherOperations(place)
            
        }) { (objectRequestOperation, error) in
            debugLog("\(error) \(objectRequestOperation)")
            self.updateCurrentWeatherOperations(place)
        }
        
        // Workaround - we need to have identificator in JSON
        operation?.setWillMapDeserializedResponseBlock { (object) -> Any? in
            guard var dictionary = object as? [String: AnyObject] else {
                debugLog("deser body is nil!")
                return object
            }
            
            dictionary["googleID"] = placeGoogleID as AnyObject
            return NSDictionary(dictionary:  dictionary)
        }
        
        objectManager?.enqueue(operation)
    }
    
    /**
     This method is called second for each place. It makes a request for **weather** API & add results to set of Weather-objects of place.
     - parameter place: place wich current weather state should be updated.
    */
    private func updateCurrentWeatherOperations(_ place: Place){
        let place = place.versionFor(placeManager.managedObjectContext)!
        
        let placeGoogleID = place.googleID
        
        let objectManager = RKObjectManager.shared()
        
        let placeLat: NSNumber! = place.latitude
        let placeLon: NSNumber! = place.longitude
        let urlRequest = objectManager?.request(with: nil, method: .GET, path: weatherPath, parameters: ["lat": placeLat, "lon": placeLon, "appid": keyAPI])

        let operation = objectManager?.managedObjectRequestOperation(with: urlRequest as URLRequest!, managedObjectContext: placeManager.managedObjectContext, success: { (objectRequestOperation: RKObjectRequestOperation?, mappingResult: RKMappingResult?) in
            if objectRequestOperation == nil {
                debugLog("objectRequestOperation == nil")
            }
            if mappingResult == nil {
                debugLog("mappingResult == nil")
            }
            self.observePlaces()
            
        }) { (objectRequestOperation, error) in
            debugLog("\(error) \(objectRequestOperation)")
            self.observePlaces()
        }
        
        // Workaround - we need to have identificator in JSON
        operation?.setWillMapDeserializedResponseBlock { (object) -> Any? in
            guard var dictionary = object as? [String: AnyObject] else {
                debugLog("deser body is nil!")
                return object
            }

            dictionary["googleID"] = placeGoogleID as AnyObject
            return NSDictionary(dictionary:  dictionary)
        }
        
        objectManager?.enqueue(operation)
    }
}
