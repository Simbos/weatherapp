//
//  WeatherManager+PlaceManagerObserver.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 28.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import Foundation

// MARK: PlaceManagerObserver
extension WeatherManager: PlaceManagerObserver {
    /// sent every time when place manager wants to modify it's context.
    func placeManagerWillChangeContent(_ placeManager: PlaceManager) {
    }
    /// sent every time when place manager has just modified it's context.
    func placeManagerDidChangeContent(_ placeManager: PlaceManager) {
    }
    /// sent every time when place manager has change current place.
    func placeManager(_ placeManager: PlaceManager, currentPlaceWasChanged currentPlace: Place?) {
        fireObserveOperation()
    }
    /// sent every time when place manager updates data of current place.
    func placeManager(_ placeManager: PlaceManager, currentPlaceWasUpdated currentPlace: Place?) {
    }
    /// sent every time when place manager inserts new place to it's context.
    func placeManager(_ placeManager: PlaceManager, newPlaceWasInserted newPlace: Place, atInterfaceIndex interfaceIndex: Int){
        fireObserveOperation()
    }
    /// sent every time when place manager removes place from it's context.
    func placeManager(_ placeManager: PlaceManager, placeWasRemoved place: Place, atInterfaceIndex interfaceIndex: Int){
    }
    /// sent every time when place manager changes interfave index of place.
    func placeManager(_ placeManager: PlaceManager, placeWasMoved place: Place, fromInterfaceIndex: Int, toInterfaceIndex: Int){
    }
    /// sent every time when place manager updates data of some place.
    func placeManager(_ placeManager: PlaceManager, placeWasUpdated place: Place, atInterfaceIndex: Int){
    }
}
