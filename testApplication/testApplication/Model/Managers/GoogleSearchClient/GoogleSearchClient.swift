//
//  GoogleSearchClient.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 28.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import Foundation

/**
 Errors of GoogleSearchClient.
*/
enum GoogleSearchClientError: Error {
    /// Some low level error from AFNetworking occurred.
    case afNetworkingError(AFHTTPRequestOperation?, Error?)
    /// Request object was catched, but it's not a NSData as expected.
    case requestObjectIsNotNSData
    /// JSONDeserialization error occurred
    case jsonDeserializationError(NSError)
    /// JSONDeserialization result could not be wraped as dictionary.
    case jsonDeserializationWrapError
    /// Some JSON-format error.
    case jsonFormatError(message: String)
}

/**
 ## GoogleSearchClient.
 GoogleSearchClient make some seacrh requests:
 
  * **searchForImageLike(:successBlock:failueBlock)** - searching image for some string-query.
*/
class GoogleSearchClient {
    /// App's Google API key.
    let APIKey: String
    /// App's Search engine identificator.
    let engineID: String
    
    /// AFNetwork's client wich actually makes requests.
    private let client: AFHTTPClient
    
    /// Share client.
    static var sharedClient: GoogleSearchClient!
    
    /**
     init client with keys
     - parameter APIKey: App's Google API key
     - parameter engineID: App's Search engine identificator.
    */
    init(APIKey: String, engineID: String){
        self.APIKey = APIKey
        self.engineID = engineID
        
        let baseURL = URL(string: "https://www.googleapis.com")
        client = AFHTTPClient(baseURL: baseURL)
    }
    
    /**
     Makes search-request for some image which could be described by some natural language string.
     - parameter query: natural language image's description.
     - successBlock: operations wich should be done in case of success.
     - failureBlock: operations wich should be done in case of failure.
    */
    func searchForImageLike(_ query: String, successBlock: @escaping (_ imageURL: URL?) -> Void, failureBlock: @escaping (_ error: GoogleSearchClientError) -> Void) {
        let parameters = [
            "key": APIKey,
            "cx": engineID,
            "searchtype": "image",
            "fields": "items",
            "start": "1",
            "q": query
        ]

        client.getPath("customsearch/v1", parameters: parameters, success: { (requestOperation, object) in
            guard let data = object as? Data else {
                failureBlock(.requestObjectIsNotNSData)
                return
            }
            let jsonObject: Any
            do {
                jsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            } catch let error as NSError {
                failureBlock(.jsonDeserializationError(error))
                return
            }
            
            guard let jsonDictionary = jsonObject as? NSDictionary else {
                failureBlock(.jsonDeserializationWrapError)
                return
            }
            
            guard let items  = jsonDictionary["items"] as? [NSDictionary] else {
                failureBlock(.jsonFormatError(message: "'items' not found"))
                return
            }
            
            guard let item = items.first else {
                successBlock(nil)
                return
            }
            
            guard let pagemap = item.value(forKey: "pagemap") as? NSDictionary else {
                failureBlock(.jsonFormatError(message: "'pagemap' not found"))
                return
            }
            
            guard let imageSources = pagemap.value(forKey: "cse_image") as? [[String : String]], imageSources.count > 0 else {
                failureBlock(.jsonFormatError(message: "'cse_image' not found"))
                return
            }
            
            guard let linkString = imageSources.first!["src"] else {
                failureBlock(.jsonFormatError(message: "'pagemap.cse_image - scrc' not found"))
                return
            }
            
            guard let url = URL(string: linkString) else {
                failureBlock(.jsonFormatError(message: "link '\(linkString)' is not an url"))
                return
            }
            
            successBlock(url)
            
        }, failure: { (requestOperation: AFHTTPRequestOperation?, error: Error?) in
                failureBlock(.afNetworkingError(requestOperation, error))
        })    
    }
}
