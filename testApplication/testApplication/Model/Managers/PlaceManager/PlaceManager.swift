//
//  PlaceManager.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 24.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreData
import GoogleMaps

/// Each observer of place manger must adopt PlaceManagerObserver
protocol PlaceManagerObserver: class {
    /// sent every time when place manager wants to modify it's context.
    func placeManagerWillChangeContent(_ placeManager: PlaceManager)
    /// sent every time when place manager has just modified it's context.
    func placeManagerDidChangeContent(_ placeManager: PlaceManager)
    /// sent every time when place manager has change current place.
    func placeManager(_ placeManager: PlaceManager, currentPlaceWasChanged currentPlace: Place?)
    /// sent every time when place manager updates data of current place.
    func placeManager(_ placeManager: PlaceManager, currentPlaceWasUpdated currentPlace: Place?)
    /// sent every time when place manager inserts new place to it's context.
    func placeManager(_ placeManager: PlaceManager, newPlaceWasInserted newPlace: Place, atInterfaceIndex interfaceIndex: Int)
    /// sent every time when place manager removes place from it's context.
    func placeManager(_ placeManager: PlaceManager, placeWasRemoved place: Place, atInterfaceIndex interfaceIndex: Int)
    /// sent every time when place manager changes interfave index of place.
    func placeManager(_ placeManager: PlaceManager, placeWasMoved place: Place, fromInterfaceIndex: Int, toInterfaceIndex: Int)
    /// sent every time when place manager updates data of some place.
    func placeManager(_ placeManager: PlaceManager, placeWasUpdated place: Place, atInterfaceIndex: Int)
}

/**
 Special Class-Container for weak references,
*/
private class PlaceManagerObserverContainer {
    weak var observer: PlaceManagerObserver?
    init(observer: PlaceManagerObserver){
        self.observer = observer
    }
}

/**
 ## Place Manager.
 Place manager observes places in some managed object context. Also, modify operations of places in context could be done by this manager.
*/
class PlaceManager: NSObject {
    
    /**
     Current user's place(location). Returns nil if this information is unavailable.
    */
    var currentPlace: Place? {
        return currentPlaceFetchedResultsController.fetchedObjects?.first
    }
    
    /**
     User's places which he stored in database.
    */
    var observablePlaces: [Place] {
        var observablePlacesCount = 0
        if let sectionsInfo = placesFetchedResultsController.sections {
            if let sectionInfo = sectionsInfo.first {
                observablePlacesCount = sectionInfo.numberOfObjects
            }
        }
        var result = [Place]()
        for row in 0 ..< observablePlacesCount {
            result.append(placesFetchedResultsController.object(at: IndexPath(row: row, section: 0)))
        }
        return result
    }
    
    /// current shared PlaceManager.
    static var sharedManager: PlaceManager!
    
    /// Observable managed object context.
    let managedObjectContext: NSManagedObjectContext
    
    /// array of manager observers.
    fileprivate var observerContainers = [PlaceManagerObserverContainer]()
    
    /// init manager for context.
    init(managedObjectContext: NSManagedObjectContext){
        self.managedObjectContext = managedObjectContext
    }
    
    /// Add new observer of manager's data.
    func addObserver(_ newObserver: PlaceManagerObserver){
        guard observerContainers.index(where: { $0.observer === newObserver }) == nil else {
            return
        }
        observerContainers.append(PlaceManagerObserverContainer(observer: newObserver))
    }

    private var _isPlaceObserving = false
    /**
     Call this method when manager is ready to observe data in database.
    */
    func startPlaceObserving(){
        if !_isPlaceObserving {
            _isPlaceObserving = true

            do{
                try placesFetchedResultsController.performFetch()
            } catch {
                fatalError("placesFetchedResultsController.performFetch throwed an error: \(error)")
            }

            do{
                try currentPlaceFetchedResultsController.performFetch()
            } catch {
                fatalError("currentPlaceFetchedResultsController.performFetch throwed an error: \(error)")
            }

            for observablePlace in observablePlaces {
                if (observablePlace.imageGlobalURL != nil) && (observablePlace.imageFileName == nil) {
                    self.tryToLoadPlaceImage(observablePlace)
                }
            }

            CurrentLocationManager.sharedManager.delegate = self
        }
    }
    
    /// internal example of **placesFetchedResultsController**
    private var _placesFetchedResultsController: NSFetchedResultsController<Place>?
    /// this **NSFetchedResultsController** controls all places which should be presented in interface by index.
    var placesFetchedResultsController: NSFetchedResultsController<Place> {
        if _placesFetchedResultsController == nil {
            let placesRequest: NSFetchRequest<Place> = Place.fetchRequest()
            placesRequest.predicate = NSPredicate(format: "interfaceIndex != nil")
            placesRequest.sortDescriptors = [NSSortDescriptor(key: "interfaceIndex", ascending: true)]
            
            let newPlacesResultsController = NSFetchedResultsController(fetchRequest: placesRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
            newPlacesResultsController.delegate = self
            _placesFetchedResultsController = newPlacesResultsController
        }
        return _placesFetchedResultsController!
    }
    /// internal example of **currentPlaceFetchedResultsController**
    private var _currentPlaceFetchedResultsController: NSFetchedResultsController<Place>?
    /// this **NSFetchedResultsController** controls current user's place/location.
    var currentPlaceFetchedResultsController: NSFetchedResultsController<Place> {
        if _currentPlaceFetchedResultsController == nil {
            let placesRequest: NSFetchRequest<Place> = Place.fetchRequest()
            placesRequest.predicate = NSPredicate(format: "isCurrentPlace == true")
            placesRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
            
            let newPlacesResultsController = NSFetchedResultsController(fetchRequest: placesRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
            newPlacesResultsController.delegate = self
            _currentPlaceFetchedResultsController = newPlacesResultsController
        }
        return _currentPlaceFetchedResultsController!
    }
    // MARK: - Place operations.
    /**
     Returns place specified by Google identificator.
     - parameter googleID: identificator of place.
     - returns: specified place or **nil** if it wasn't found.
     */
    func placeByGoogleID(_ googleID: String) -> Place? {
        return Place.placeInContext(managedObjectContext, withGoogleID: googleID)
    }
    
    /**
     Removes place from database.
     - parameter position: position of place in interface.
    */
    func removePlaceFromPosition(_ position: Int){
        let place = observablePlaces[position]
        managedObjectContext.perfomBlockOnPrivateContext{ managedObjectContext in
            managedObjectContext.delete(place.versionFor(managedObjectContext)!)
        }
    }
    
    /**
     Changes interface position of some place.
     - parameter fromPosition: source position of place.
     - parameter toPosition: destination position of place
    */
    func changePlaceInterfacePosition(_ fromPosition: Int, toPosition: Int){
        var places = observablePlaces
        let place = places.remove(at: fromPosition)
        places.insert(place, at: toPosition)
        
        managedObjectContext.perfomBlockOnPrivateContext{ managedObjectContext in
            for (index, place) in places.enumerated() {
                let place = place.versionFor(managedObjectContext)!
                place.interfaceIndex = NSNumber(value: index)
            }
        }
    }
    
    /**
     Inserts new place to database.
     - parameter place: new Google place.
    */
    func insertNewPlace(_ place: GMSPlace) {
        var insertedPlace: Place?
        managedObjectContext.perfomBlockOnPrivateContextAndWait{ managedObjectContext in
            let newPlace = NSEntityDescription.insertNewObject(forEntityName: Place.entityName, into: managedObjectContext) as! Place
            newPlace.googleID  = place.placeID            
            if let localityIndex = place.addressComponents?.index(where: { $0.type == "locality" }){
                newPlace.name = place.addressComponents![localityIndex].name
            } else {
                newPlace.name = "current location"
            }
            newPlace.latitude  = NSNumber(value: place.coordinate.latitude)
            newPlace.longitude = NSNumber(value: place.coordinate.longitude)
            let maxInterfaceIndex = Place.maxInterfaceIndexInContext(managedObjectContext) ?? -1
            newPlace.interfaceIndex = NSNumber(value: maxInterfaceIndex + 1)
            newPlace.isCurrentPlace = false
            
            insertedPlace = newPlace
        }
        insertedPlace = insertedPlace?.versionFor(managedObjectContext)
        startImageSearch(insertedPlace!)
    }
    
    /**
     Starts searching image for some place.
     - parameter place: place wich should have an image.
    */
    func startImageSearch(_ place: Place){
        GoogleSearchClient.sharedClient.searchForImageLike(place.name, successBlock: { (imageURL) in
            if let urlString = imageURL?.absoluteString {
                self.managedObjectContext.perfomBlockOnPrivateContextAndWait{ managedObjectContext in
                    let place = place.versionFor(managedObjectContext)!
                    place.imageGlobalURL = urlString
                }
                self.tryToLoadPlaceImage(place)
            }
            }) { (error) in
                
        }
    }
    
    /**
     Tries to load image for some place.
    */
    private func tryToLoadPlaceImage(_ place: Place){
        let place = place.versionFor(managedObjectContext)!
        let request =  URLRequest(url: URL(string: place.imageGlobalURL!)!)
        let operation = AFImageRequestOperation(request: request) { (image) in
            self.managedObjectContext.perfomBlockOnPrivateContext{ managedObjectContext in
                let place = place.versionFor(managedObjectContext)!
                place.image = image
            }
        }
        operation?.start()
    }
}

extension PlaceManager: NSFetchedResultsControllerDelegate {
    /// sent when NSFetchedResultsController will change it's context
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        _ = observerContainers.map{
            $0.observer?.placeManagerWillChangeContent(self)
        }
    }
    
    /// sent when is changing some object in context
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        if controller === placesFetchedResultsController {
            switch type {
            case .insert:
                _ = observerContainers.map{
                    $0.observer?.placeManager(self, newPlaceWasInserted: anObject as! Place, atInterfaceIndex: newIndexPath!.row)
                }
                
            case .delete:
                _ = observerContainers.map{
                    $0.observer?.placeManager(self, placeWasRemoved: anObject as! Place, atInterfaceIndex: indexPath!.row)
                }
                
            case .move:
                _ = observerContainers.map{
                    $0.observer?.placeManager(self, placeWasMoved: anObject as! Place, fromInterfaceIndex: indexPath!.row, toInterfaceIndex: newIndexPath!.row)
                }
                
            case .update:
                _ = observerContainers.map{
                    $0.observer?.placeManager(self, placeWasUpdated: anObject as! Place, atInterfaceIndex: indexPath!.row)
                }
            }
        } else if controller === currentPlaceFetchedResultsController {
            switch type {
            case .insert,.delete,.move:
                _ = observerContainers.map{
                    $0.observer?.placeManager(self, currentPlaceWasChanged: anObject as? Place)
                }
            case .update:
                _ = observerContainers.map{
                    $0.observer?.placeManager(self, currentPlaceWasUpdated: anObject as? Place)
                }
            }
        }
    }
    
    /// sent when NSFetchedResultsController has changed it's context
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        _ = observerContainers.map{
            $0.observer?.placeManagerDidChangeContent(self)
        }
        
        observerContainers = observerContainers.filter{
            $0.observer != nil
        }
    }
}
