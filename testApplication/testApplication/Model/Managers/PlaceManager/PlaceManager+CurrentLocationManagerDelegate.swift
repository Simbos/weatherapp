//
//  PlaceManager+CurrentLocationManagerDelegate.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 28.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreData
import GoogleMaps

extension PlaceManager: CurrentLocationManagerDelegate {
    // MARK: - CurrentLocationManagerDelegate
    /// sent every time when current location manager updates current place.
    func currentLocationManager(_ currentLocationManager: CurrentLocationManager, didUpdateCurrentPlace place: GMSPlace?) {
        managedObjectContext.perfomBlockOnPrivateContextAndWait{ managedObjectContext in
            if let currentPlace = Place.currentPlaceInContext(managedObjectContext){
                currentPlace.isCurrentPlace = false
            }
            
            guard let place = place else {
                return
            }
            
            if let storedPlace = Place.placeInContext(managedObjectContext, withGoogleID: place.placeID){
                storedPlace.isCurrentPlace = true
            } else {
                let newPlace = NSEntityDescription.insertNewObject(forEntityName: Place.entityName, into: managedObjectContext) as! Place
                if let localityIndex = place.addressComponents?.index(where: { $0.type == "locality" }){
                    newPlace.name = place.addressComponents![localityIndex].name
                } else {
                    newPlace.name = "current location"
                }
                newPlace.googleID = place.placeID
                newPlace.latitude = NSNumber(value: place.coordinate.latitude)
                newPlace.longitude = NSNumber(value: place.coordinate.longitude)
                newPlace.isCurrentPlace = true
            }
        }
        
        if (currentPlace != nil) && (currentPlace!.imageGlobalURL == nil) {
            startImageSearch(currentPlace!)
        }
    }
}
