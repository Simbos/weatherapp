//
//  CurrentLocationManager.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 23.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps

/// Every delegate of CurrentLocationManager must adopt **CurrentLocationManagerDelegate**.
protocol CurrentLocationManagerDelegate: class {
    /// sent every time when current location manager updates current place.
    func currentLocationManager(_ currentLocationManager: CurrentLocationManager, didUpdateCurrentPlace place: GMSPlace?)
}

/**
 ## CurrentLocationManager.
 CurrentLocationManager is using CoreLocation & Google Services to control current's user place/location.
*/
class CurrentLocationManager: NSObject {
    /// used location manager.
    private var locationManager = CLLocationManager()
    /// current user's location
    fileprivate(set) var currentLocation: CLLocation? {
        didSet{
            if currentLocation != nil {
                startPlaceSearch()
            } else {
                delegate?.currentLocationManager(self, didUpdateCurrentPlace: nil)
            }
        }
    }
    /// Google Service client
    private var placesClient = GMSPlacesClient()
    
    /// place name of current user's location
    private(set) var currentPlace: GMSPlace?
    
    /// delegate object
    weak var delegate: CurrentLocationManagerDelegate?
    
    /// private hidden constructor.
    private override init(){
        super.init()
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.delegate = self
        
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined,.denied,.restricted:
            ()
        case .authorizedWhenInUse,.authorizedAlways:
            locationManager.startUpdatingLocation()
        }
    }
    
    /// this method check current authorization status of location manager and tries to start updating location.
    func checkLocationManager() {
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    /// shared singleton.
    static let sharedManager = CurrentLocationManager()
    
    /// Called when Manager is ready to search place by coordinate.
    private func startPlaceSearch(){
        placesClient.currentPlace(callback: currentPlaceSearchDidFinish)
    }
    
    /// Called when Google service has finished place search operation.
    private func currentPlaceSearchDidFinish(_ placeLikelihoodList: GMSPlaceLikelihoodList?, error: Error?){
        guard error == nil else {
            debugLog("error is not nil: \(error!)")
            return
        }
        
        let placeLikelihoodList = placeLikelihoodList ?? GMSPlaceLikelihoodList()
        
        let placeLikelihood = placeLikelihoodList.likelihoods.max { (likelihood1, likelihood2) -> Bool in
            likelihood1.likelihood < likelihood2.likelihood
        }
        
        if placeLikelihood?.place.placeID != currentPlace?.placeID {
            currentPlace = placeLikelihood?.place
            delegate?.currentLocationManager(self, didUpdateCurrentPlace: currentPlace)
        }
    }
}

extension CurrentLocationManager: CLLocationManagerDelegate {
    //MARK: - CLLocationManagerDelegate
    /// sent when location manager has new location-values
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        if let currentLocation = locations.last {
                if self.currentLocation != nil {
                    if currentLocation.distance(from: self.currentLocation!) > 500 {
                        self.currentLocation = currentLocation
                    }
                } else {
                    self.currentLocation = currentLocation
                }
        }
    }
}
