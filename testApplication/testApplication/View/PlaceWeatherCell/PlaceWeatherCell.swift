//
//  PlaceWeatherCell.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 24.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import UIKit

/**
 This cell outputs information about weather for some place.
*/
class PlaceWeatherCell: UITableViewCell {
    /// Outputs image of place.
    @IBOutlet var placeImageView: UIImageView!
    /// Outputs background chart image
    @IBOutlet var backgroundGradientView: UIImageView!
    /// Outputs forecast chart.
    @IBOutlet var weatherChart: WeatherChart!
    /// Outputs name of place.
    @IBOutlet var placeNameLabel: UILabel!
    /// Outputs current temperature for place.
    @IBOutlet var currentTemperatureLabel: UILabel!
    
    /// Some preparations after cell is loaded from nib.
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.layer.cornerRadius = 10.0
    }
    
    static var gradientImage: UIImage?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if backgroundGradientView.image == nil {
            if PlaceWeatherCell.gradientImage == nil {
                PlaceWeatherCell.gradientImage = Gradient.gradientImageWithSize(bounds.size, colors: [UIColor.clear, UIColor.black])
            }
            backgroundGradientView.image = PlaceWeatherCell.gradientImage
        }
    }
}
