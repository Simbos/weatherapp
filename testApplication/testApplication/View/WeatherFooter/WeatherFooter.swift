//
//  WeatherFooter.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 28.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import UIKit

/**
 Weather footer keeps some controls after weather forecast table.
*/
class WeatherFooter: UIView {
    /// This button is used by user to add new place in table.
    @IBOutlet var addButton: UIButton!
    /// This button is used by user to change editable state of table.
    @IBOutlet var editButton: UIButton!
    
    /// Creates a new footer from a NIB file.
    class func loadFromNib() -> WeatherFooter {
        let nibArray = Bundle.main.loadNibNamed("WeatherFooter", owner: nil, options: nil)
        return nibArray!.first as! WeatherFooter
    }
}
