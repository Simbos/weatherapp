//
//  PlaceSuggestionCell.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 24.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import UIKit

/**
 **PlaceSuggestionCell** outputs inforamation about place-suggestions.
*/
class PlaceSuggestionCell: UITableViewCell {
    /// Text view wich is used to output place's name.
    @IBOutlet var textView: UITextView!
}
