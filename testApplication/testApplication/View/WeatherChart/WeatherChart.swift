//
//  WeatherChart.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 27.07.16.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import UIKit

/// width of all lines in chart.
private let lineWidth: CGFloat = 1.0
/// used to draw any lines in chart.
private let borderColor = UIColor.white
/// used to fill any polygons in chart.
private let fillColor =  UIColor.white.withAlphaComponent(0.5)

/// width of labels in chart
private let labelWidth: CGFloat = 30.0
/// height of labels in chart
private let labelHeight: CGFloat = 10.0
/// font of labels in chart
private let labelFont = UIFont.boldSystemFont(ofSize: 8.0)
/// color of labels in chart.
private let labelColor = UIColor.white

/// TemperatureLevel** keeps information about temperature and it's coordinate representation on chart.
private typealias TemperatureLevel = (yCoordinate: CGFloat, temperature: Double)

/// **HourLine** keeps information about some hour-date and it's coordinate representation on chart.
private typealias HourLine = (xCoordinate: CGFloat, date: Date)

/**
 This function is used to generate chart & all titles information of chart.
 - parameter weatherArray: array of tuples with temperatures and specified dates.
 - parameter imageSize: size of image to be generated.
 - returns: tuple with image, temperature levels and hour lines.
*/
private func generateImage(_ weatherArray:[(temperature: Double, datetime: Date)], imageSize: CGSize) -> (image: UIImage?, temperatures: [TemperatureLevel], hours: [HourLine]) {
    var resultImage: UIImage?
    var levelDescriptions = [TemperatureLevel]()
    var hourDescriptions = [HourLine]()
    
    guard weatherArray.count > 0 else {
        return (image: resultImage, temperatures: levelDescriptions, hours: hourDescriptions)
    }
        
    let minTemperature = weatherArray.min(by: { weather1, weather2 -> Bool in weather1.temperature < weather2.temperature })!.temperature
    let chartMinTemperature = (minTemperature < 0.0) ? minTemperature : 0.0
    let maxTemperature = weatherArray.max(by: { weather1, weather2 -> Bool in weather1.temperature < weather2.temperature })!.temperature
    let chartMaxTemperature = (maxTemperature > 0.0) ? maxTemperature : 0.0
    
    let sortedWeatherArray = weatherArray.sorted{ weather1, weather2 -> Bool in weather1.datetime.compare(weather2.datetime) == .orderedAscending }
    
    /// Internal func. Returns Y-coordinate for some temperature
    func yCoordinateForTemperature(_ temperature: Double) -> CGFloat {
        return CGFloat((chartMaxTemperature - temperature) * Double(imageSize.height - 2*labelHeight) / (chartMaxTemperature - chartMinTemperature)) + labelHeight
    }
    /// Internal func. Returns X-coordinate for some date.
    func xCoordinateForDatetime(_ datetime: Date) -> CGFloat {
        return CGFloat((datetime.timeIntervalSince1970 - sortedWeatherArray.first!.datetime.timeIntervalSince1970) * Double(imageSize.width - labelWidth - 5.0) / (sortedWeatherArray.last!.datetime.timeIntervalSince1970 - sortedWeatherArray.first!.datetime.timeIntervalSince1970))
    }
    
    /// Internal func. Returns image coordinate for some weather tuple.
    func pointForWeather(_ weather: (temperature: Double, datetime: Date)) -> CGPoint {
        let x = xCoordinateForDatetime(weather.datetime)
        let y = yCoordinateForTemperature(weather.temperature)
        
        return CGPoint(x: x, y: y)
    }
    
    UIGraphicsBeginImageContextWithOptions(imageSize, false, UIScreen.main.nativeScale)
    let graphicContext = UIGraphicsGetCurrentContext()!
    graphicContext.setLineCap(.round)
    graphicContext.setLineWidth(lineWidth)
    var borderRed = CGFloat(0)
    var borderGreen = CGFloat(0)
    var borderBlue = CGFloat(0)
    var borderAlpha = CGFloat(0)
    borderColor.getRed(&borderRed, green: &borderGreen, blue: &borderBlue, alpha: &borderAlpha)
    graphicContext.setStrokeColor(red: borderRed, green: borderGreen, blue: borderBlue, alpha: borderAlpha)
    var fillRed = CGFloat(0)
    var fillGreen = CGFloat(0)
    var fillBlue = CGFloat(0)
    var fillAlpha = CGFloat(0)
    fillColor.getRed(&fillRed, green: &fillGreen, blue: &fillBlue, alpha: &fillAlpha)
    graphicContext.setFillColor(red: fillRed, green: fillGreen, blue: fillBlue, alpha: fillAlpha)
        
    graphicContext.beginPath()
    
    let zeroLevel = yCoordinateForTemperature(0.0)
    
    let points = sortedWeatherArray.map{ pointForWeather($0) }
    
    // polygon
    graphicContext.move(to: CGPoint(x: points.last!.x, y: points.last!.y))
    graphicContext.addLine(to: CGPoint(x: points.last!.x, y: zeroLevel))
    graphicContext.addLine(to: CGPoint(x: points.first!.x, y: zeroLevel))
    
    for point in points {
        graphicContext.addLine(to: CGPoint(x: point.x, y: point.y))
    }
    graphicContext.fillPath(using: .evenOdd)
    graphicContext.strokePath()
    
    // polygon border
    graphicContext.move(to: CGPoint(x: points.last!.x, y: points.last!.y))
    graphicContext.addLine(to: CGPoint(x: points.last!.x, y: zeroLevel))
    graphicContext.addLine(to: CGPoint(x: points.first!.x, y: zeroLevel))
    
    for point in points {
        graphicContext.addLine(to: CGPoint(x: point.x, y: point.y))
    }
    graphicContext.strokePath()
        
    //levels
    let minLine = Int((chartMinTemperature / 5) - 1)
    let maxLine = Int((chartMaxTemperature / 5) + 1)
    
    let levelIndexes = Array(minLine ..< maxLine)
    
    levelDescriptions = levelIndexes.map{
        let temperature = Double($0) * 5.0
        let level = yCoordinateForTemperature(temperature)
        return (yCoordinate: level, temperature: temperature)
    }
    
    for levelDescription in levelDescriptions {
        graphicContext.move(to: CGPoint(x: points.first!.x, y: levelDescription.yCoordinate))
        graphicContext.addLine(to: CGPoint(x: points.last!.x, y: levelDescription.yCoordinate))
    }
    graphicContext.strokePath()
        
    // houres
    var hour = sortedWeatherArray.first!.datetime.nextHourDate
    var hourIndex = 0
    while hour.compare(sortedWeatherArray.last!.datetime) == .orderedAscending {
        defer{
            hour = hour.nextHourDate
            hourIndex += 1
        }
        
        let xCoordinate = xCoordinateForDatetime(hour)
        if hourIndex % 2 == 1 {
            hourDescriptions.append((xCoordinate: xCoordinate, date: hour))
        } else {
            graphicContext.move(to: CGPoint(x: xCoordinate, y: imageSize.height - labelHeight))
            graphicContext.addLine(to: CGPoint(x: xCoordinate, y: imageSize.height - labelHeight / 2.0))
        }
    }
    graphicContext.strokePath()
    
    resultImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    return (image: resultImage, temperatures: levelDescriptions, hours: hourDescriptions)
}

/**
 ## WeatherChart.
 Ouputs weather forecast chart.
 
 use **redrawChart(weatherArray:)** to ouput new chart.
 
 use **freeChart** to clear chart.
 
 use **temperatureUnit** to change displayed temperature units.
*/
class WeatherChart: UIImageView {
    /// Current temperature units.
    var temperatureUnit: TemperatureUnit = .celsius
    
    /// Some preparations after initizialization.
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = 5.0
    }
    
    /**
     Internal func. Generates temperature label.
     - parameter temperature: title's temperature value.
     - returns: new generated label.
    */
    private func labelForTemperature(_ temperature: Double) -> UILabel {
        let label = UILabel()
        label.text = String(format:"%.1f%@",temperature, temperatureUnit.unitString)
        label.font = labelFont
        label.textColor = labelColor
        label.textAlignment = .right
        return label
    }
    
    /**
     Internal func. Generates time label.
     - parameter hour: title's date value.
     - returns: new generated label.
    */
    private func labelForHour(_ hour: Date) -> UILabel {
        let label = UILabel()
        let formatter        = DateFormatter()
        formatter.dateFormat = "hh:mm"
        label.text = formatter.string(from: hour)
        label.font = labelFont
        label.textColor = labelColor
        label.textAlignment = .center
        return label
    }
    
    /// This offset is used for temperature titles on right side of chart.
    private var labelsOffsetX: CGFloat {
        return bounds.width - labelWidth - 5.0
    }
    
    /// This offset vis used for time titles on bottom of chart.
    private var labelsOffsetY: CGFloat {
        return bounds.height - labelHeight
    }
    
    /// Clears chart.
    func freeChart(){
        DispatchQueue.main.async{
            for subview in self.subviews {
                subview.removeFromSuperview()
            }
            self.image = nil
        }
    }
    
    /**
     Redraws chart with new values.
    */
    func redrawChart(_ weatherArray:[(temperature: Double, datetime: Date)]){
        
        let imageSize   = self.bounds.size

        DispatchQueue.global().async {
            
            let content = generateImage(weatherArray, imageSize: imageSize)

            DispatchQueue.main.async{
                for subview in self.subviews {
                    subview.removeFromSuperview()
                }
                
                for levelDescription in content.temperatures {
                    guard levelDescription.yCoordinate < self.labelsOffsetY else {
                        continue
                    }
                    
                    let label = self.labelForTemperature(levelDescription.temperature)
                    self.addSubview(label)
                    
                    label.frame = CGRect(x: self.labelsOffsetX, y: levelDescription.yCoordinate - labelHeight / 2.0, width: labelWidth, height: labelHeight)
                }
                
                for hourDescription in content.hours {
                    guard (hourDescription.xCoordinate - labelWidth / 2.0) > 0 else {
                        continue
                    }
                    
                    let label = self.labelForHour(hourDescription.date)
                     self.addSubview(label)
                    label.frame = CGRect(x: hourDescription.xCoordinate - labelWidth / 2.0 , y: self.labelsOffsetY, width: labelWidth, height: labelHeight)
                }
                
                self.image = content.image
                
                self.layoutIfNeeded()
            }
        }
    }
    
}

private extension Date {
    /// Returns next nearest hour.
    var nextHourDate: Date {
        let calendar = Calendar.current
        var components = (calendar as NSCalendar).components([.era,.year,.month,.day,.hour], from: self)
        components.hour =  components.hour! + 1
        return calendar.date(from: components)!
    }
}
