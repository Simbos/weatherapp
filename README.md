# What is this?

Hello.

This repository contains the project of simple weather iOS application.

# Used services

- current weather is fetched by [Open Weather Map - Weather](http://openweathermap.org/current)
- forecast weather is fetched by [Open Weather Map - Forecast 5](http://openweathermap.org/forecast5)
- coordinates transform to places, seacrhing for places. [Google places](https://developers.google.com/places)
- image searching.[Google custom search](https://developers.google.com/custom-search)

# RESTkit?

OK, guys, I' ve done this app with RESTkit. It was hard, painfull, trolling work full of workarounds. Next time I would rather prefer a couple of many small frameworks than one big RESTKit:

- Networking - [Alamofire](https://github.com/Alamofire/Alamofire)
- RESTfull API - [Siesta](https://github.com/bustoutsolutions/siesta) or [Moya](https://github.com/Moya/Moya)
- Mapping - [Moya-SwiftyJSONMApper](https://github.com/AvdLee/Moya-SwiftyJSONMapper) or [Sync](https://github.com/hyperoslo/Sync)

# Architecture

Basically app is written in MVC with Apple's "Big Fat Controllers". But a lot of code is stored in model. There are two basic model classes:

- **PlaceManager**. Place manager keeps place model(current place + user's observable places) and notify it's observers about changes in that model. Also it has some contol methods for Get/Insert/Delete/Replace operations.
- **WeatherManger**. Weather manager observes place manager's model. On change - it refetch all weather data for stored places. Also, it refetch data on timer fire operation. 

# Documentation
Class reference is presented in `\docs` folder.

# What else? How to make app better?

My time is running out, but I also wanted to:

- separate Active-Model & Control methods of mangers to different classes.
- add a special model for **PlaceSearch** cache & special controller.
- add App reactions for App-State changes.
- add App reactions for APP-ability changes.
- refactor web-requests to NSOperationQueue-pattern
